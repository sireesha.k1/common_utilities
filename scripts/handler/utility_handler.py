from pymongo import MongoClient

myclient = MongoClient('143.110.191.155', 37217)
db = myclient["sireeshak"]
Collection = db["user_detail"]


class Utilities:
    @staticmethod
    def insertion(content):
        if isinstance(content, list):
            Collection.insert_many(content)
            print("Inserted")
        else:
            Collection.insert_one(content)

    @staticmethod
    def find_one_document():
        return str(Collection.find_one())

    @staticmethod
    def find_document():
        collection_documents = []
        for document in Collection.find():
            collection_documents.append(document)
        return str(collection_documents)

    @staticmethod
    def update_document():
        collection_documents = []
        Collection.update_many({"gender": "Female"}, {"$set": {"gender": "female"}})
        collection = Collection.find()
        for document in collection:
            collection_documents.append(document)
        return str(collection_documents)

    @staticmethod
    def delete_document():
        collection_documents = []
        Collection.delete_many({"last_name": {"$regex": "^H"}})
        collection = Collection.find()
        for document in collection:
            collection_documents.append(document)
        return str(collection_documents)
