from flask import Blueprint, request
from scripts.handler.utility_handler import Utilities

mongo_json = Blueprint("json_in_mongo_blueprint", __name__)


@mongo_json.route('/', methods=['POST'])
def postJsonHandler1():
    content = request.get_json()
    Utilities.insertion(content)
    return str(content)


@mongo_json.route('/findonedata', methods=['GET'])
def postJsonHandler2():
    return Utilities.find_one_document()


@mongo_json.route('/finddata', methods=['GET'])
def postJsonHandler3():
    return Utilities.find_document()


@mongo_json.route('/updatedata', methods=['GET'])
def postJsonHandler4():
    return Utilities.update_document()


@mongo_json.route('/deletedata', methods=['GET'])
def postJsonHandler5():
    return Utilities.delete_document()
